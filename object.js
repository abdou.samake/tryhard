import {getHostel, getHostelObject} from './hotels.data';

console.log(getHostelObject());
console.log(getHostel);

// faire tous les exos de la page arrays en utilisant object.keys ou object.values

// exercice 0 : mettre une majuscule à toutes les RoomName

console.log('[ARRAY]Exo0 : ---------------------------');
function capi(name) {
  return name.charAt(0).toUpperCase() + name.slice(1);
}
let hostel0 = Object.values(getHostelObject());
hostel0 = hostel0.map((hotel) => {
  hotel.rooms = Object.values(hotel.rooms)
      .map((room) => {
        room.roomName = capi(room.roomName);
        return room;
      });
  return hotel;
});
console.log(hostel0);
// exercice 1 : trier les hotels par nombre
// de chambres (plus grand en 1er) et créer un tableau
// contenant seulement
// le nom des hotels dans leur ordre de tri
console.log('[ARRAY]Exo1 : ', '');
let hostel1 = Object.values(getHostelObject());
hostel1 = hostel1.sort((a, b) => b.roomNumbers - a.roomNumbers)
    .reduce((acc, hotel) => acc.concat(hotel.name), []);
console.log(hostel1);
// exercice 2 : faire un tableau avec toutes les chambres de tous les hotels,
// et ne garder que les chambres qui
// ont plus que 3 places ou exactement 3 places et
// les classer par ordre alphabétique selon le non de la chambre

console.log('[ARRAY]Exo2 : ', '');
let hostel2 = Object.values(getHostelObject());
hostel2 = hostel2.reduce((acc, hotel) => acc.concat(Object.values(hotel.rooms)
    .filter((room) => room.size >= 3)
    .sort((a, b) => a.roomName < b.roomName ? -1 : +1)), []);
console.log(hostel2);
// exercice 3 : faire un tableau avec toutes les chambres des hotels
// et ne garder que les chambres qui ont plus de 3 places et dont le
// nom de chambre a une taille supérieure à 15 charactères

console.log('[ARRAY]Exo3 : ', '');
let hostel3 = Object.values(getHostelObject());
hostel3 = hostel3.reduce((acc, hotel) => acc.concat(Object.values(hotel.rooms)
    .filter((room) => room.size > 3 && room.roomName.length > 15)), []);
console.log(hostel3);
// exercice 4 : enlever de la liste des hotels toutes les chambres qui ont plus
// de 3 places et changer la valeur de roomNumbers pour qu'elle reflete
// le nouveau nombre de chambres

console.log('[ARRAY]Exo4 : ', '');
let hostel4 = Object.values(getHostelObject());
hostel4 = hostel4.map((hotel) => {
  hotel.rooms = Object.values(hotel.rooms)
      .filter((room) => room.size <= 3);
  hotel.roomNumbers = hotel.rooms.length;
  return hotel;
});
console.log(hostel4);
// exercice 5  : extraire du tableau hostels l'hotel qui a le nom 'hotel ocean' en le supprimant du tableau,
// et le mettre dans une nouvelle variable
// puis effacer toutes ses chambres et mettre à jour sa valeur room number, puis pusher l'hotel modifié dans hostels
// puis faire un sort par nom d'hotel
// puis donner le nouvel index de l'hotel océan (faire 2 méthodes : avec indexOf et avec un foreach)

console.log('[ARRAY]Exo5 : ', '');
const hostel5 = Object.values(getHostelObject());
const index = hostel5.findIndex((hotel) => hotel.name === 'hotel ocean');
const [hoteleToDelete] = hostel5.splice(index, 1);
hoteleToDelete.rooms = [];
hoteleToDelete.roomNumbers = hoteleToDelete.rooms.length;
hostel5.push(hoteleToDelete);
hostel5.sort((a, b) => a.name < b.name ? -1 : +1);
const newIndex = hostel5.findIndex((hotel) => hotel.name === 'hotel ocean');
let newIndex2;
hostel5.forEach((hotel, index) => {
  if (hotel.name === 'hotel ocean') {
    newIndex2 = index;
  }
  return newIndex2;
});
console.log(hostel5, index, hoteleToDelete, newIndex, newIndex2);
// exercice 6 : créer un objet dont les clés sont le nom des hotels et dont la valeur est un booléen qui indique
// si l'hotel a une chambre qui s'appelle 'suite marseillaise'

console.log('[ARRAY]Exo6 : ', '');
const myObject = {};
const hostel6 = Object.values(getHostelObject());
hostel6.forEach((hotel) => myObject[hotel.name] = Object.values(hotel.rooms)
    .some((room) => room.roomName === 'suite marseillaise'));
// exercice 7 : faire une fonction qui prend en paramètre un id d'hotel et qui retourne son nom

console.log('[ARRAY]Exo7 : ', '');
const hostel7 = Object.values(getHostelObject());
function hotelNameById(hotelId) {
  const hotelToFind = hostel7.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel inconnu`;
  }
  return hotelToFind.name;
}
console.log(hotelNameById(1));
console.log(hotelNameById(2));
console.log(hotelNameById(3));
// exercice 8 : faire une fonction qui prend en paramètre un id d'hotel et un id de chambre et qui retourne son nom

console.log('[ARRAY]Exo8 : ', '');
const hostel8 = Object.values(getHostelObject());
function roomNameById(hotelId, roomId) {
  const hotelToFind = hostel8.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel inconnu`;
  }
  const roomToFind = Object.values(hotelToFind.rooms).find((room) => room.id === roomId);
  if (!roomToFind) {
    return `chambre inconnue`;
  }
  return roomToFind.roomName;
}
console.log(roomNameById(1, 2));
// exercice 9: faire une fonction qui prend en paramètre la liste des hotels
// et qui vérifie que toutes les chambres des hotels ont bien
// une majuscule a leur nom et qui renvoie un boolean donnant le résultat.
// Puis faire en sorte que la liste des hotels valide bien cette fonction.

console.log('[ARRAY]Exo9 : ', '');
console.log('[ARRAY]Exo9 avec maj: ', '');
const hostel9 = getHostelObject();
function capiAllRoom(hotels) {
  return Object.values(hotels).every((hotel) => Object.values(hotel.rooms)
      .every((room) => room.roomName === capi(room.roomName)));
}
console.log(capiAllRoom(hostel9));
// exercice 10 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie un tableau contenant les id des hotels
// qui ont au moins une chambre avec plus ou exactement 5 places

console.log('[ARRAY]Exo10 : ', '');
const hostel10 = getHostelObject();
function returnHotelId(hotels) {
  return Object.values(hotels).reduce((acc, hotel) => {
    const roomToSelect = Object.values(hotel.rooms).find((room) => room.size >= 5);
    return roomToSelect ? acc.concat(hotel.id) : acc;
  }, []);
}
console.log(returnHotelId(hostel10));

// exercice 11 : faire une fonction qui prend en paramètre la liste des hotels
// et qui renvoie dans un tableau toutes les chambres qui ont plus de 3 places
// et qui sont dans un hotel avec piscine
// puis qui ajoute à chaque chambre une propriété "hotelName"
// qui contient le nom de l'hotel à laquelle elle appartient

console.log('[ARRAY]Exo11 : ', '');
const hostel11 = getHostelObject();
function allRoomInHotelPoolTrue(hotels) {
  return Object.values(hotels).reduce((acc, hotel) => acc.concat(Object.values(hotel.rooms))
      .filter((room) => room.size >3 && hotel.pool === true)
      .map((room) => {
        room.hotelName = hotel.name;
        return room;
      }), []);
}
console.log(allRoomInHotelPoolTrue(hostel11));
// exercice 12 : faire une fonction qui prend en paramètre la liste des hotel,
// un id d'hotel et une taille et qui renvoie le nom et l'id de toutes
// les chambres de cet hotel qui font exactement la taille indiquée

console.log('[ARRAY]Exo12 : ', '');
const hostel12 = getHostelObject();
function allRoomHotel(hotels, hotelId, size) {
  const hotelToFind = hostel8.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel inconnu`;
  }
  if (typeof size !== 'number') {
    return `size doit être un nombre`;
  }
  return Object.values(hotelToFind.rooms).filter((room) => room.size === size)
      .map((room) => ({roomName: room.roomName, id: room.id}));
}
console.log(allRoomHotel(hostel12, 1, 2));

// exercice 13 : faire une fonction qui prend en paramètre la liste des hotels et
// un id d'hotel et qui supprime cet hotel de la liste des hotels

console.log('[ARRAY]Exo13 : ', '');
const hostel13 = getHostelObject();
function deleteHotel(hotels, hotelId) {
  const hotelToFind = Object.values(hotels).find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel inconnu`;
  }
  return Object.values(hotels).filter((hotel) => hotel.id !== hotelId);
}
console.log(deleteHotel(hostel13, 1));
// exercice 14 : faire une fonction qui prend en paramètre la liste des hotel et
// un id d'hotel et un id de chambre et qui supprime cet chambre de l'hotel concerné

console.log('[ARRAY]Exo14 : ', '');
const hostel14 = Object.values(getHostelObject());
function deleteRoomByListe(hotels, hotelId, roomId) {
  const hotelToFind = hotels.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel inconnu`;
  }
  const roomToFind = Object.values(hotelToFind.rooms).find((room) => room.id === roomId);
  if (!roomToFind) {
    return `chambre inconnue`;
  }
  hotelToFind.rooms = Object.values(hotelToFind.rooms).filter((room) => room.id !== roomId);
  hotelToFind.roomNumbers = Object.values(hotelToFind.rooms).length;
  return hotels;
}
console.log(deleteRoomByListe(hostel14, 1, 2));
// exercice 15 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et un boolean et qui va changer la valeur de l'attribut "pool"
// dans l'hotel concerné avec le boolean donné
console.log('[ARRAY]Exo15 : ', '');
const hostel15 = getHostelObject();
function changeHotelPool(hotels, hotelId, pool) {
  const hotelToFind = Object.values(hotels).find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel inconnu`;
  }
  hotelToFind.pool = pool;
  return hotels;
}
console.log(changeHotelPool(hostel15, 2, true));
// exercice 16 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel et une nouvelle chambre (avec son nom et sa taille) qui
// ajoute la nouvelle chambre dans l'hote concerné et lui donne un id qui suit le
// dernier id de l'hotel

console.log('[ARRAY]Exo16 : ', '');
const hostel16 = Object.values(getHostelObject()).map((hotel) => {
  hotel.rooms = Object.values(hotel.rooms);
  return hotel;
});
function addNewRoom(hotels, hotelId, newRoom) {
  const hotelToFind = hotels.find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel inconnu`;
  }
  hotelToFind.rooms.push(newRoom);
  hotelToFind.roomNumbers = hotelToFind.rooms.length;
  return hotels;
}
console.log(addNewRoom(hostel16, 1, {roomName: 'suite Ramadan', size: 25}));
// exercice 17 : faire une fonction qui prend en paramètre la liste des hotels,
// une id d'hotel, un id de chambre et un nouveau nom de chambre qui va changer le nom
// de la chambre indiquée par le nouveau nom

console.log('[ARRAY]Exo17 : ', '');
const hostel17 = Object.values(getHostelObject());
function changeNameRoom(hotels, hotelId, roomId, newName) {
  const hotelToFind = Object.values(hotels).find((hotel) => hotel.id === hotelId);
  if (!hotelToFind) {
    return `hotel inconnu`;
  }
  const roomToFind = Object.values( hotelToFind.rooms).find((room) => room.id === roomId);
  if (!roomToFind) {
    return `chambre inconnue`;
  }
  roomToFind.roomName = newName;
  return hotels;
}
console.log(changeNameRoom(hostel17, 1, 1, 'Ramadan Mubarack'));

// Faire les exercices suivants :

const lessons = {
  lesson1: true,
  lesson11: false,
  lesson2: true,
  lesson15: false,
  lesson3: true,
  lesson4: true,
  lesson13: false,
  lesson6: false,
  lesson12: false,
  lesson7: false,
  lesson8: false,
  lesson9: false,
  lesson10: false,
  lesson5: false,
  lesson14: false,
};

console.log(lessons);

// exercice 1 : changer cet objet pour que ses clées soient classées dans l'ordre alphabétiques

// exercice 2 : en gardant l'objet trié, faites une fonction qui
// donne le numéro de la première clé qui a la valeur false
// sous la forme {key: 4, value: false}
